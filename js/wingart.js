
var tabsBox 	= $(".tabs"),
	carousel 	= $('.carousel'),
	datepicker	= $(".datepicker"),
	raty 		= $(".rating");

if(tabsBox.length){
  include("js/easyResponsiveTabs.js");
}

if (carousel.length){
	include("js/owl.carousel.js");
}

if ($(".phone-mask").length){
	include("js/jquery.mask.js");	
}

if (raty.length){
	include("js/jquery.raty.js");	
}

if (datepicker.length){
	include("js/jquery-ui.min.js");	
	include("js/datepicker-ru.js");
}

if ($(".main-slider").length || $(".magazine-carousel").length){
	include("js/swiper.js");
}

function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}


$(document).ready(function(){

	if ($("input[name='avatar-file']").length){
		$("input[name='avatar-file']").on('change', function(){
			var avatarSection = $(this).closest('.form-avatar'),
				avatarImg     = avatarSection.find('img');

			avatarImg.attr('src', 'images/'+$(this)[0].files[0].name+'');

		})
	}


	if ($(".main-slider").length){

		var swiper = new Swiper('.main-slider', {
	        pagination: '.swiper-pagination',
	        slidesPerView: 2,
	        centeredSlides: true,
	        paginationClickable: true,
	        initialSlide : 1,
	        spaceBetween: 0,
	        speed : 1000,
	        nextButton: '.swiper-button-next',
        	prevButton: '.swiper-button-prev',
        	// Responsive breakpoints
			  breakpoints: {
			    767: {
			      slidesPerView: 1,
			    },
			    // when window width is <= 640px
			    995: {
			      slidesPerView: 2,
			      initialSlide : 0,
			      centeredSlides: false
			    }
			  }
	    });
	}


	if ($(".magazine-carousel").length){

		var magazineCarousel = new Swiper('.magazine-carousel', {
	        pagination: '.magazine-pagination',
	        slidesPerView: 1,
	        centeredSlides: false,
	        paginationClickable: true,
	        initialSlide : 0,
	        spaceBetween: 0,
	        speed : 1000,
	        nextButton: '.magazine-button--next',
        	prevButton: '.magazine-button--prev'
	    });

	}

	$(".faq").on('click', "dt", function(){
		var currentItem = $(this),
			currentList = currentItem.parent();


		if (!currentItem.hasClass('active')){
			currentList.find('dd').slideUp();
			currentList.find('.active').removeClass('active');
			currentItem.addClass('active').next().slideDown();
		}
		else{
			currentItem.removeClass('active').next().slideUp(200);
		}
		
	})

	if ($("#city-confirmation").length){
		$("#city-confirmation").arcticmodal();
	}

	if (raty.length){
		raty.each(function() {
			$(this).raty({ score: $(this).data('score'), path: 'images'  });
		})
	}

	if (datepicker.length){
		datepicker.datepicker({
			regional : [ "ru" ]
		});
	}

	if ($(".phone-mask").length){
		$(".phone-mask").mask("+7 (000) 000-00-00", {placeholder: "+7 (___) ___-__-__"});;
	}
	
	// responsive navigation toggle button
	$("#resp-toggle-btn").on('click', function(){
	  	$("body").toggleClass('navTrue');
	  	$("nav .menu").slideToggle();
	})

	if ($(window).width() < 996){
		$(document).on("click", function (event) {
	      if ($(event.target).closest('#resp-toggle-btn').length) return;
	      $("body").removeClass('navTrue');
		  $("nav .menu").slideUp();
	      event.stopPropagation();
	    });
	}


	// init of tabs
	if (tabsBox.length){
		tabsBox.each(function(){
			var currentTabs = $(this);

			currentTabs.easyResponsiveTabs();
		})
	}

	// init of carousel
  	if (carousel.length){
	  	carousel.each(function(){
	  		var currentCarousel = $(this),
	  			items 			= currentCarousel.data('items'),
	  			singleItem      = false,
	  			itemsDesc 		= currentCarousel.data('items-desc'),
	  			itemsDescSm		= currentCarousel.data('items-desc-sm'),
	  			itemsTab 		= currentCarousel.data('items-tab');

	  		if (items < 1){
	  			return false;
	  		}

	  		if (items == 1){
	  			singleItem = 1;
	  			itemsDesc = 1;
		  			itemsDescSm = 1;
	  			itemsTab = 1;
	  		}

	  		currentCarousel.owlCarousel({
	  			items : items,
	  			singleItem : singleItem,
	  			itemsDesktop : [1199, itemsDesc],
		        itemsDesktopSmall : [979, itemsDescSm],
		        itemsTablet : [768, itemsTab],
		        itemsTabletSmall : false,
		        itemsMobile : [479, 1],
		        navigation : true,
		        navigationText : [" ", " "],
		        transitionStyle : "fadeUp",
		        autoHeight : true
	  		})
	  	})
	}


	$(".form__tabs").on('click', ".form__tabs-nav li", function(e){
		var currentItem 		= $(this),
			currentTabsNav 		= currentItem.parent(),
			parentTabs 			= $(e.delegateTarget),
			currentTabsContent	= parentTabs.find('.form__tabs-content');


		currentItem.parent().find('.active').removeClass('active');
		currentItem.addClass('active');


		currentTabsContent.find(">*").hide();
		currentTabsContent.find(">*").eq(currentItem.index()).fadeIn();


	})




})